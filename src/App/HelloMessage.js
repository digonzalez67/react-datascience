import React from "react";
import * as d3 from "d3";
import '../css/main.sass';

export class HelloMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            datos : "",
            departaments : []
        };
        this.changeValue = this.changeValue.bind(this);
        this.loadCityes();
    }
    changeValue ( value ) {
        console.log(value.target.value)
        this.setState({
            datos : value.target.value
        })
    }
    loadCityes () {
        let self = this;
        fetch( 'http://localhost:3050/departamento' , {"method":"GET",} )
            .then( response => response.json() )
            .then( response => self.setState( { departaments : response }) )

    }
    render() {
        return (
            <div className={"container"}>
                <div className="row">
                    <div className="col-lg-6">{this.state.datos}</div>
                    <div className="col-lg-6"><input type="text" className={"form-control"} value={this.state.datos} onChange={this.changeValue} /></div>
                    <select name="" id="" className='form-control'>
                        ${ this.state.departaments.map( (departament, i) => {
                            return <option key={i} value={departament.departamento}> {departament.departamento}</option>
                        })}
                    </select>

                </div>
            </div>
        );
    }
}