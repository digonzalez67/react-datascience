import React from "react";
// import {Chart} from "./Chart";
import Chart from "react-google-charts";


export class Filters extends React.Component {

    constructor (props) {
        super(props);
        this.state              = {departments : [] , municipalities : [] , dataChart : [] , value : {  department : '' , municipality : '' }};
        this.changeDepartment   = this.changeDepartment.bind(this);
        this.changeMunicipality = this.changeMunicipality.bind(this);
        this.loadDepartment();
    }

    loadDepartment () {
        let self = this;
        fetch( 'http://localhost:3050/departamento' , {"method":"GET",} )
            .then( response => response.json() )
            .then( response => self.setState( { departments : response }) )
    }

    loadMunicipalities (departamento) {
        let self = this;
        fetch( 'http://localhost:3050/municipio/'+departamento , {"method":"GET",} )
            .then( response => response.json() )
            .then( response => self.setState( { municipalities : response }) )
    }

    changeDepartment(select) {
        this.setState( {value : { department : select.target.value }} )
        this.loadMunicipalities(select.target.value);
    }

    changeMunicipality (select) {
        this.setState( {value : { municipality : select.target.value }} )

        let self = this;

        let tablas = ['arma_empleada', 'dia', 'edad', 'sexo', 'hora', 'escolaridad', 'modelo'];
        tablas.map( tabla => {
            fetch( `http://localhost:3050/hurtos/${this.state.value.department}/${select.target.value}/${tabla}` ,
                {"method":"GET",} )
                .then( response => response.json() )
                .then( response => {

                    let title = null;
                    let arrayData =  response.map( ( dat , i ) => {
                        title = Object.keys(dat);
                        return Object.values(dat);
                    });

                    if ( arrayData.length > 0 ) {
                        let dataChart = self.state.dataChart;
                        dataChart[tabla] = [title].concat(arrayData);
                        console.log( dataChart )
                        console.log( dataChart.length )
                        console.log( Object.values(dataChart) )
                        self.setState({ dataChart : dataChart })
                    }
                });
        })
    }


    render () {
        return <div className="row">
            <div className="col-lg-6">
                <label htmlFor="">
                    Departamentos
                </label>
                <select name="" id="" className='form-control' onChange={this.changeDepartment} value={this.state.value.department}>
                    <option key={0} value={0}> Seleccione el departamento</option>
                    { this.state.departments.map( (department, i) => {
                        return <option key={i} value={department.id}> {department.nombre}</option>
                    })}
                </select>
            </div>
            <div className="col-lg-6">
                <label htmlFor="">
                    Municipio
                </label>
                <select name="" id="" className="form-control" onChange={this.changeMunicipality} value={this.state.value.municipality}>
                    <option key={0} value={0}> Seleccione el municipio</option>
                    { this.state.municipalities.map( (municipality, i) => {
                        return <option key={i} value={municipality.id}> {municipality.nombre}</option>
                    })}
                </select>
            </div>
                { Object.values(this.state.dataChart).map ( ( data , i ) => {
                    let title = Object.keys(this.state.dataChart)[i]
                    return <div className="col-lg-6 mt-5" key={i}>
                            <Chart
                            width={'100%'}
                            height={300}
                            chartType="ColumnChart" //BubbleChart
                            loader={<div>Hurtos ${title} </div>}
                            data={data}
                            options={{
                                intervals: { style: 'sticks' },
                                legend: 'none',
                                vAxis: {
                                    title: title,
                                },
                            }}
                        />
                    </div>
                } )
                }
        </div>
    }
}