import React from "react";
import rd3 from 'react-d3-library';
import * as d3 from "d3";
const RD3Component = rd3.Component;
const PieChart = rd3.PieChart;


export class Chart extends React.Component {

    constructor (props) {
        super(props);
        this.state = {d3: ''}
        this.myRef = React.createRef();
    }

    componentDidMount () {

        let h = 400;
        let accessToRef = d3.select(this.myRef.current)
            .append('svg')
            .attr("width", 500)
            .attr('height' ,h)
            .style("padding","10")
            .style('margin-left',50);

        console.log(Object.values( this.props.data_city.map ( data => {
            return data.Dia;
        }).reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null)) ) );

        accessToRef.selectAll("rect")
            /*.data(Object.values(this.props.data_city.map ( data => {
                return data.Dia;
            }).reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null))))*/
            .data([5,12,6,8,10])
            .enter()
            .append('rect')
            .attr("x" , ( d,i) => i * 70 )
            .attr("y" , ( d,i) => h -10 * d )
            .attr("width" , 65 )
            .attr("height" , ( d,i) => d * 10 )
            .attr("fill" , "tomato" )


    }


    render () {
        return <div className="row">
            <div ref={this.myRef}></div>
        </div>
    }
}