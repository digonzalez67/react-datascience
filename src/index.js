import React from "react";
import * as ReactDOM from "react-dom";
import {Filters} from "./App/Filters";
import './css/main.sass';
import RFA from 'react-fontawesome';


ReactDOM.render(
    <div className="container py-2">
        <div className="text-center">
            <RFA name="chart-pie"
                 size="5x"
                 style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }} />
        </div>
        <Filters/>
    </div>
    ,
    document.querySelector('.root')
);